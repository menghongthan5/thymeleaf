package org.ksga.springboot.homeworkthymeleaf.controller;
import org.ksga.springboot.homeworkthymeleaf.model.Article;
import org.ksga.springboot.homeworkthymeleaf.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class ArticalController {
    private ArticleService articleService;

    @Autowired
    public void setArticleService(ArticleService articleService){
        this.articleService = articleService;
    }

    @GetMapping("/")
    public String index(Model model){
        List<Article> articles = articleService.findAllArticle();
        model.addAttribute("articles",articles);
        return "index";
    }
}
