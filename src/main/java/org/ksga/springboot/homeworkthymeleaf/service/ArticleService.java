package org.ksga.springboot.homeworkthymeleaf.service;

import org.ksga.springboot.homeworkthymeleaf.model.Article;

import java.util.List;

public interface ArticleService {
    List<Article> findAllArticle();
    Article findArticleByid(int id);
    boolean creatArticle(Article article);
    boolean updateArticle(int id, Article newArticle);
    boolean deletArticle(int id);
}
