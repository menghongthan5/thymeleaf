package org.ksga.springboot.homeworkthymeleaf.service;

import org.ksga.springboot.homeworkthymeleaf.model.Article;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

    @Service
public class ArticleServiceImpl implements ArticleService{

    private List<Article> articles = new ArrayList<>(
           Arrays.asList ( new Article(1, "Rolls Royce ចេញឡានថ្មីតម្លៃថ្លៃបំផុតលើលោក ទិញបាន Lexus LX570 ៣២០គ្រឿង", "ក្រុមហ៊ុនផលិតរថយន្តអង់គ្លេស Rolls Royce បានធ្វើឲ្យមនុស្សគ្រប់គ្នាភ្ញាក់ផ្អើលខ្លាំង បន្ទាប់ពីប្រកាសចេញម៉ូដែលថ្មី ហៅថា Boat Tail ដែលជារថយន្តថ្មីមានតម្លៃថ្លៃបំផុតលើលោក។" , "https://cdn.sabay.com/cdn/media.sabay.com/media/sabay-news/Technology-News/Long-Tech/F33/60b0616897670_1622172000_medium"))
    );

    @Override
    public List<Article> findAllArticle() {
        return null;
    }
    @Override
    public Article findArticleByid(int id) {

        for (Article article : articles){
            if(article.getId() == id){
                return article;
            }
        }

        return null;
    }

    @Override
    public boolean creatArticle(Article article) {
      return articles.add(article);

    }

    @Override
    public boolean updateArticle(int id, Article newArticle) {
        int index = -1;

        for (int i =0; i<articles.size(); i++)
        {
            if (articles.get(i).getId() ==id){
                index = 0;
                break;
            }
        }
        articles.set(index, newArticle);
        return false;
    }

    @Override
    public boolean deletArticle(int id) {
        return false;
    }
}
